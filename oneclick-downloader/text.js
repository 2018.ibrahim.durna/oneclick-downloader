function t(inputID, returnOnly) {
    var translation = chrome.i18n.getMessage(inputID);
    var string = (translation === '') ? inputID : translation;
    var element = document.getElementById(inputID);

    if (returnOnly === true)
        return string;

    if (element === null)
        return;

    // FIXME: Do some actual identification instead of this …
    if (element.value === undefined) {
        element.innerHTML = string;
    } else {
        element.value = string;
    }
}

document.addEventListener('DOMContentLoaded', function() {

    // Hosters info
    t('hostersHeader');
    t('listHostersButton');
    t('customHostersButton');
    t('backToOptionsButton');

    // Path options
    t('optionsPath');
    t('optionsDownloadPathNone');
    t('optionsDownloadPathDate');
    t('optionsDownloadCustomPathNote');
    t('optionsDownloadPathRequest');

    // Startup options
    t('optionsStartupBehavior');
    t('optionsStartupBehaviorDefault');
    t('optionsStartupBehaviorCurrent');
    t('optionsStartupBehaviorLinks');

    // Schema options
    t('optionsSchemas');
    t('optionsFilenameSchemaInfo');

    // Options navigation
    t('optionsSave');
    t('optionsReset');

    // Overall
    t('downloaderAuto');
    t('downloaderRestart');

    // Tabs
    t('tabsHeader');
    t('tabsScrape');
    t('tabsRescan');
    t('tabsCheckAll');
    t('tabsUncheckAll');

    // Links
    t('linksNotShown');
    t('linksDownload');
    t('linksCheckAll');
    t('linksUncheckAll');

    // Download Path
    t('downloadPathDescription');
    t('downloadPathPrefix');
    t('downloadDefaultDownloadPath');

    // Download Buttons
    t('downloadClose');
    t('downloadRestart');

    // Variables
    t('variablesHelpOpen');
    t('variablesHelpHeader');
    t('variablesHelpClose');

    // Custom hosters
    t('customHostersInfo');

    // Farewell
    t('farewellText')

});
