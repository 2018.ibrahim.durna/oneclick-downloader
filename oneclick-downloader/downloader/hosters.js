// PREAMBLE
//
// You can suggest hosters or provide a complete definition (the object and the
// function) for any hoster you want. Simply file an issue on GitHub or contact
// me via the contact form in Google Chrome Store or simply write me an e-mail
// to spam@0x7be.de to reach me.
//
// Hosters should be actual one click image hosters and similar and no private
// websites or fanpages/galleries. Exceptions can be made if there are any
// benefits in it.
//
//
// THE OBJECT
//
// Each definition has to push an object to supportedHosters[] containing all
// necessary meta information an identifications attributes. The following
// items are mandatory.
//
// hoster:    This value holds the official name of the hosting service and is
//            for informational purposes only. Try to correctly determine and
//            use the official name of the service.
//
// hostname:  The actual host name on which the images are hosted. This has to
//            be the basic host name without any subdomains. For example, if
//            the images are hosted at img.example.com the hostname value has
//            to be example.com without the “img.” part.
//
// pattern:   A Javascript regular expression part (without any modifiers) that
//            matches the links the hoster uses. This should be as simple as
//            possible to prevent breakage or simply not matching any links.
//            For example the hoster uses http://example.com/image.php?id=123
//            as image link the pattern should be only match the part up the ID
//            part: “example\\.com/image\\.php\\?id=”.
//            The pattern testing is case insensitive and is greedy by default
//            so you don’t need to add that in special cases. You need to
//            double-escape special characters like quesion marks or the dot
//            but you don’t need to escape slashes.
//
// function:  The name of the function to be used for matched links. Of course
//            a corresponding function has to be created in order to work.
//
// author:    The name or nickname of the author who provided the definition.
//            This gets shown on the definitions overview page.
//
// contact:   Contact information (e-mail or website URL) of the author. If
//            you don’t want any contact information being shown simply use
//            the null value instead.
//
// modified:  Date when the definition was first added or updated after it was
//            initially added.
//
// comment:   A short comment on – for example – where the definition is used
//            or what to keep in mind when using it.
//
// idea:      An optional object holding information about who suggested
//            this hoster via what channel and when.
//            {'name': 'John Doe',
//             'channel': 'GitHub',
//             'date': '2015-11-15'}
//
//
// THE FUNCTION
//
// Functions have access to three values. c, u and b. c is the content of the
// currently processed image page as DOM structure and u is the URL of the
// image page that is currently processed. If you want to only access u you
// need to add c, too. Just add them as parameters in your function definition
// to use them within the function.
//
// An additional parameter called b can be used. b is the Base64-encoded ASCII 
// string of the full link HTML as string. It is encoded because it is passed 
// around using HTML. In order to use b it needs to be decoded and made into
// an object again.
//
// function foobar (c, u, b) {
//   var decoded = b.bToObj()
// }
//
//
// PROTOTYPE FUNCTIONS
//
// Functions have access to four string-related prototype functions.
//
// string.getLast() returns the last part of a slash separated value. For
// example an URL or file path.
//
//   > var s = 'http://www.example.com/foo/bar/baz.jpg';
//   > s.getLast();
//   = 'baz.jpg'
//
// string.getHost() returns the hostname of a given URL. This might break on
// multi-part TLDs (co.uk, com.au, etc.)
//
//   > var h = 'http://www.example.com/foo/bar/baz.jpg'
//   > h.getHost();
//   = 'example.com'
//
// string.startsWith() determines wether the given string starts with the
// given string or not.
//
//   > var h = 'Hello World!'
//   > h.startsWith('Hel');
//   = true
//   > h.startsWith('blah');
//   = false
//
// string.decode() decodes the given string. Currently this is practically
// an alias for the built-in javascript function decodeURI() in further
// versions it might check for other stuff as well.
//
//   > var n = 'Image%20%281%29.jpg'
//   > n.decode()
//   = 'Image (1).jpg'
//
// use_og_image(c) returns the Open Graph Protocol `og:image` value in the
// form that is needed to be returned by a hoster’s function. So when the
// hoster uses the Open Graph Protocol simply do the following for the hoster
// function
//
// function my_hoster_function(c) {
//     return return_og_image(c);
// }
//
//
// Feel free to check this documentation from time to time to see if there are
// more functions available that can replace some code in your definition and
// then simply re-submit it so I can update it.
//
//
// THE RETURN VALUE
//
// All definitions have to return an object containing the full image URL and
// the actual image file name.
//
// url:   The URL of the image should be the URL of the full-size image that
//        was determined by your function. When downloading the document
//        referred to by the URL the result should be an image file.
//
// name:  Try to extract the actual file name from the provided DOM structure
//        or the image page URL and strip all additional data added by the
//        hoster. For example: id123_theFileName_fullsize.jpg should result in
//        theFileName.jpg
//
// Desist from using a hard-coded filename because there are usually multiple
// images being downloaded and not only one at a time.




supportedHosters = [];




supportedHosters.push({
    'hoster': 'ImageBam',
    'hostname': 'imagebam.com',
    'pattern': 'imagebam\\.com/image/',
    'function': 'imagebam',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2019-05-17',
    'comment': 'Very common on celebrity image boards'
});

function imagebam(c) {
    return return_og_image(c);
}




supportedHosters.push({
    'hoster': 'imgbox',
    'hostname': 'imgbox.com',
    'pattern': '/imgbox\\.com/',
    'function': 'imgbox',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2019-07-13',
    'comment': 'Very common on celebrity image boards'
});

function imgbox(c) {
    return {
        'url': return_og_image(c).url,
        'name': c.getElementById('img').title
    }
}




supportedHosters.push({
    'hoster': 'ImageVenue',
    'hostname': 'imagevenue.com',
    'pattern': '\\.imagevenue\\.com/img\\.php\\?',
    'function': 'imagevenue',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2015-05-15',
    'comment': 'Very common on celebrity image boards'
});

function imagevenue(c) {
    var url = c.getElementById('thepic').src;
    var name = url.getLast();
    var pattern = '^[0-9]*_(.*)_[0-9]*_[0-9]*[a-zA-Z]*.([a-zA-Z]*)$';
    var regex = new RegExp(pattern, 'g');
    return {
        'url': url,
        'name': name.replace(regex, '$1.$2')
    };
}




supportedHosters.push({
    'hoster': 'imgur',
    'hostname': 'imgur.com',
    'pattern': '://(i\\.)?imgur\\.com/',
    'function': 'imgur',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2015-05-16',
    'comment': 'Widely used on Reddit'
});

function imgur(c, u) {
    var direct = u.includes('/i.imgur.com/');
    var res = {};

    if (direct === true) {
        res['url'] = u;
        res['name'] = u.getLast();
    } else {
        var div = c.getElementsByClassName('post-image')[0];
        var img = div.getElementsByTagName('img')[0].src;
        res['url'] = img;
        res['name'] = img.getLast();
    }

    return res;
}




supportedHosters.push({
    'hoster': 'TurboImageHost',
    'hostname': 'turboimagehost.com',
    'pattern': 'turboimagehost\\.com/',
    'function': 'turboimagehost',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2015-06-09',
    'comment': 'Used on some celebrity image boards'
});

function turboimagehost(c) {
    return return_og_image(c);
}




supportedHosters.push({
    'hoster': 'Facebook Pages Galleries',
    'hostname': 'facebook.com',
    'pattern': 'facebook\\.com\\/[0-9a-zA-Z\\.-]+\\/photos\\/a\\.[0-9]+\\/.*\\?type=[0-9]+$',
    'function': 'facebookPages',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2015-06-15',
    'comment': 'Page galleries except title images gallery'
});

function facebookPages(c, u) {
    var url = c.querySelectorAll('[data-ploi]')[0].dataset.ploi
    var name = url.getLast().replace(/\?_.*/g, '')

    return { 'url': url, 'name': name };
}




supportedHosters.push({
    'hoster': 'flickr',
    'hostname': 'flickr.com',
    'pattern': 'flickr\\.com/photos/:?[a-z0-9_\\-\\.@]*/[0-9]*/$',
    'function': 'flickrAlbum',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2015-07-03',
    'comment': 'VERY nasty plaintext parsing of JS code and very wonky'
});

function flickrAlbum(c, u) {
    var variants = [];
    var url = '';
    var modelExportText = c.getElementsByClassName('modelExport')[0].innerText;
    var modelExport = modelExportText.replace(/(\r\n|\n|\r)/gm, '').split('"');

    for (var i = 0; i < modelExport.length; i++) {
        if (modelExport[i].indexOf('\\/\\/farm') === 0)
            variants.push(modelExport[i]);
    }

    url = u.split(':')[0] + ':' + variants.pop();

    return {
        'url': url.replace(/\\/g, ''),
        'name': url.getLast()
    };
}




supportedHosters.push({
    'hoster': 'Issuu PDF Downloader',
    'hostname': 'abuouday.com',
    'pattern': 'issuu-downloader\\.abuouday\\.com/single\\.php\\?id=.*',
    'function': 'IssuuPdfDownloader',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2015-11-15',
    'comment': 'Online PDF to image converter',
    'idea': { 'name': 'jaredbidlow', 'channel': 'GitHub', 'date': '2015-11-12' }
});

function IssuuPdfDownloader(c) {
    var url = c.getElementById('img').src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'Hotflick Image Upload',
    'hostname': 'hotflick.net',
    'pattern': 'hotflick.net/././\\?q=.*',
    'function': 'hotflick',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2016-10-29',
    'comment': 'Used on some celebrity image boards',
    'idea': { 'name': 'GitBoudewijn', 'channel': 'GitHub', 'date': '2016-10-29' }
});

function hotflick(c) {
    var url = c.getElementById('img').src;
    var pattern = '^[a-zA-Z]*_[0-9]*_(.*)_[0-9]*_[0-9]*[a-zA-Z]*.([a-zA-Z]*)$';
    var regex = new RegExp(pattern, 'g');
    return {
        'url': url,
        'name': url.getLast().replace(regex, '$1.$2')
    };
}




supportedHosters.push({
    'hoster': 'SomeImage',
    'hostname': 'someimage.com',
    'pattern': '://(www\\.)?someimage.com/[0-9a-zA-Z]*',
    'function': 'someimage',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'modified': '2016-10-29',
    'comment': 'Used on some celebrity image boards',
    'idea': { 'name': 'GitBoudewijn', 'channel': 'GitHub', 'date': '2016-10-29' }
});

function someimage(c) {
    var url = c.getElementById('viewimage').src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name.decode()
    };
}




supportedHosters.push({
    'hoster': 'reddit',
    'hostname': 'redd.it',
    'pattern': '://i.redd.it/[0-9a-zA-Z]*',
    'function': 'redd_it',
    'author': 'Dirk Sohler',
    'contact': 'https://0x7be.de',
    'modified': '2019-06-09',
    'comment': 'Used on Reddit',
});

function redd_it(c, u) {
    return {
        'url': u,
        'name': u.getLast()
    };
}




supportedHosters.push({
    'hoster': 'ImgCredit.XYZ',
    'hostname': 'wwv.imgcredit.xyz',
    'pattern': 'wwv\\.imgcredit\\.xyz/image/',
    'function': 'imgcredit',
    'author': 'Arhey',
    'contact': 'https://github.com/freearhey',
    'modified': '2017-12-24',
    'comment': 'Very common on celebrity image boards'
});

function imgcredit(c) {
    var link = c.getElementsByClassName('btn-download')[0]
    var url = link.href;
    var name = link.download;
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'ImageTwist',
    'hostname': 'imagetwist.com',
    'pattern': 'imagetwist\\.com/',
    'function': 'imagetwist',
    'author': 'Arhey',
    'contact': 'https://github.com/freearhey',
    'modified': '2018-01-03',
    'comment': 'Very common on celebrity image boards'
});

function imagetwist(c, u) {
    var img = c.getElementsByClassName('pic')[0];
    var url = img.src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'PiXhost',
    'hostname': 'pixhost.to',
    'pattern': 'pixhost\\.to/show/',
    'function': 'pixhost',
    'author': 'Arhey',
    'contact': 'https://github.com/freearhey',
    'modified': '2018-03-29',
    'comment': 'Very common on celebrity image boards'
});

function pixhost(c) {
    var img = c.getElementById('image')
    var url = img.src;
    var name = img.alt;
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'IMX.to',
    'hostname': 'imx.to',
    'pattern': 'imx\\.to/[u,i]/',
    'function': 'imx_to',
    'author': 'Dirk Sohler',
    'contact': 'https://0x7be.de',
    'modified': '2020-08-01',
    'comment': 'Only links to IMX.to are supported! No IMX.to image pages!',
    'idea': { 'name': 'Christian Elbrianno', 'channel': 'GitLab', 'date': '2019-06-09' }
});

function imx_to(c, u, b) {
    var link = b.bToObj()
    var thumbnail = link.getElementsByTagName('img')[0].src
    var regex = /\/\/imx\.to\/([u,i])\/(t)/
    return {
        'url': thumbnail.replace(regex, "//i.imx.to/i"),
        'name': c.title.replace('IMX.to / ', '')
    }
}




supportedHosters.push({
    'hoster': 'Postimage.org',
    'hostname': 'postimg.cc',
    'pattern': 'postimg\\.cc/[0-9a-zA-Z]*/?',
    'function': 'postimg',
    'author': 'Dirk Sohler',
    'contact': 'https://0x7be.de',
    'modified': '2019-07-13',
    'comment': '',
    'idea': { 'name': 'n/a', 'channel': 'Chrome Web Store', 'date': '2017-07-30' }
});

function postimg(c) {
    return return_og_image(c);
}
