function downloadImages () {

    log = document.getElementById('downloadLog');

    var links = document.getElementsByClassName('linkInfo');
    var wantedLinks = [];

    for (var i=0; i < links.length; i++) {
        if(links[i].type !== 'checkbox')
            continue;

        if (links[i].checked !== true)
            continue;

        getPage(links[i]);
    }
}


function getPage(linkObject) {
    var xhr = new XMLHttpRequest();
    var url = linkObject.value;
    xhr.open('GET', url, true);
    xhr.responseType = 'document';
    xhr.onload = function(e) {
        downloadImage(this.response, url.getHost(), linkObject);
    };

    xhr.send();
}


function downloadImage(c, hostname, linkObject) {
    var imageData = false;
    var url = linkObject.value;
    var btoa = linkObject.dataset.btoa;
    var counter = 0;

    for (var i=0; i < supportedHosters.length; i++) {
        var hoster = supportedHosters[i];
        var regex = new RegExp(hoster['pattern'], 'i');
        if (regex.test(url) === true) {
            // … and GO!
            imageData = window[hoster['function']](c, url, btoa);
            break;
        }
    }

    if (imageData === false)
        return;

    // Determine Path
    var path = document.getElementById('downloadPath').value;
    path = replaceVariables(path, path, linkObject)
    path = (path.length == 0) ? '' : path + '/';

    // Determine file name and replace variables
    var filenameSchema = document.getElementById('filenameSchema').value;
    filename = replaceVariables(filenameSchema, imageData['name'], linkObject)

    chrome.downloads.download({
        'url': imageData['url'],
        'filename': path + filename,
        'saveAs': false // gets ignored for unknown reason
    }, function (object) {});

}


document.addEventListener('DOMContentLoaded', function() {


    chrome.storage.sync.get({'filenameSchema': '%full%'}, function(o) {
        var filenameSchema = document.getElementById('filenameSchema');
        filenameSchema.value = o.filenameSchema;
    });

    downloadClose = document.getElementById('downloadClose');
    downloadClose.addEventListener('click', function() {
        window.close();
    });

    downloadRestart = document.getElementById('downloadRestart');
    downloadRestart.addEventListener('click', function() {
        location.reload();
    });

    variablesHelpOpen = document.getElementById('variablesHelpOpen');
    variablesHelpOpen.addEventListener('click', function() {
        var variablesHelp = document.querySelector('#variablesHelpDialog');
        appendVariablesHelp('variablesHelpContent');
        variablesHelp.showModal();
    });

    variablesHelpClose = document.getElementById('variablesHelpClose');
    variablesHelpClose.addEventListener('click', function() {
        var variablesHelp = document.querySelector('#variablesHelpDialog');
        variablesHelp.close();
    });

});

chrome.downloads.onCreated.addListener(function (object) {
    var ol = document.getElementById('downloadLog');
    var li = document.createElement('li');
    var a = document.createElement('a');
    var text = '[' + object['url'].getHost() + '] ' + object['url'].getLast();
    a.appendChild(document.createTextNode(text));
    a.href = object['url'];
    li.id = 'download-'+object['id'];
    li.className = object['state'];
    li.appendChild(a);
    ol.appendChild(li);
});


chrome.downloads.onChanged.addListener(function (object) {
    if (object['state'] !== undefined) {
        var entry = document.getElementById('download-'+object['id']);
        entry.className = object['state']['current'];
    }
});
